# Deep Dive into Docker

###### by @rawkode

------

## Docker Fundamentals

- [Creating Containers, Part I](1-fundamentals/1.1-creating-containers-part-1.md)
- [Official Images](1-fundamentals/1.2-official-images.md)
- [Creating Containers, Part II](1-fundamentals/1.3-creating-containers-part-2.md)
- [Building Images, Part I](1-fundamentals/1.4-building-images-part-1.md)
- [Building Images, Part II](1-fundamentals/1.5-building-images-part-2.md)
- [Publishing images](1-fundamentals/1.6-publishing-images.md)

## Docker Orchestration in Development

- [Docker Compose](3-orchestration-in-development/3.1-docker-compose.md)

## Advanced Docker

- Optimising Images
- Networking
- Volumes
- Events

## Docker Orchestration in Production

- Swarm Mode
